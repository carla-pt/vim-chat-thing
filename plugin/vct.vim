let s:homedir = expand('<sfile>:h:h')
" let s:job = ""
let s:command = ""
let s:arg1 = ""

function! StartChatThing()
  set splitbelow
  exec "find ".s:homedir."/chats/chat_file"
  setfiletype chat-thing
  syntax on

  new
  resize 4
  exec "let s:job = job_start('python ".s:homedir."/python/bot.py', {'out_cb': 'BotHandler'})"

  nnoremap <CR> :call SendMessage()<CR>
  inoremap <CR> <ESC>:call SendMessage()<CR>i

  "temporary to fix in lsqrts side
  set virtualedit=all
  set t_Co=256

 
endfunction

function! BotHandler(channel, msg)
  call writefile([a:msg], s:homedir."/chats/chat_file", "a")
  exec "normal! \<c-w>k"
  e
  exec "norm! G\<c-e>"
  exec "normal! \<c-w>j"
  
  let s:msg = split(a:msg)

  if len(s:msg)>1 && strpart(s:msg[1], 0, 1) ==? "."
    call s:CommandHandler(s:msg)
  endif
endfunction


function! SendMessage()
  let s:message = join(getline(1, '$'))
  norm! dd
  call BotHandler(s:job, ch_evalraw(s:job, s:message."\n"))
endfunction

function! s:CommandHandler(msg)
  let s:sender = a:msg[0]
  if len(a:msg) < 3 
    call BotHandler(s:job, "Err: wrong command")
    return
  endif

  let s:command = a:msg[1]
  let s:args = a:msg[2: ]

  if s:command ==? ".colour" || s:command ==? ".color" 
    call s:color(s:sender, s:args)
  endif

  if s:command ==? ".latex"
    call s:latex(s:sender, s:args)
  endif

endfunction

function! s:color(sender, args)
  let s:arg = str2nr(a:args[0])
  if s:arg <= 0 || s:arg > 255
    call BotHandler(s:job, "Err: arg1 ∉ [1, 255] ∩ Z")
    return
  endif

  if s:sender ==? "Carla_:"
    exec "hi Carla ctermfg=".s:arg
    exec "tabnew ".s:homedir."/syntax/chat-thing.vim"
    exec "norm! gg/Carla\<CR>nf=lcw".s:arg."\<ESC>"
    :wq
  else
    exec "hi lsqrt ctermfg=".s:arg
    exec "tabnew ".s:homedir."/syntax/chat-thing.vim"
    exec "norm! gg/lsqrt\<CR>nf=lcw".s:arg."\<ESC>"
    :wq
  endif

endfunction

function! s:latex(sender, args)
  if a:args[0] ==? "start"
    exec "tabnew ".s:homedir."/latex/file.tex"
    exec "0r ".s:homedir."/latex/skeleton.tex"
    LLPStartPreview

    "replace by better
    norm gt
  endif



endfunction
