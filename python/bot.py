import os
import threading
import discord
import asyncio
import time

vimMessage = ""
botDown = True

class vimInputHandler(threading.Thread):
    def run(self):
        global vimMessage, botDown
        while(botDown):
            time.sleep(0.1)

        while 1:
            vimMessage = input()

vimhandler = vimInputHandler()
vimhandler.start()

from dotenv import load_dotenv
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

async def my_background_task():
    global vimMessage
    await client.wait_until_ready()
    channel = client.get_channel(690882993744510996)
    while not client.is_closed():
        if(vimMessage != ""):
            await channel.send(vimMessage)
            vimMessage = ""
        await asyncio.sleep(0.1)


@client.event
async def on_message(message):
    print(message.author.name + ": " + message.content, flush=True)


@client.event
async def on_ready():
    global botDown
    print("Bot up", flush=True)
    botDown = False

client.loop.create_task(my_background_task())
client.run(TOKEN)
